package br.com.projeto.adaptadores;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

interface PropriedadeMensageiro {

    Properties paraPropriedades();

    final class PropriedadeConsumidor implements PropriedadeMensageiro{

        private final String host;

        PropriedadeConsumidor(final String host) {
            this.host = host;
        }

        @Override
        public Properties paraPropriedades() {

            final Properties properties = new Properties();

            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "open-banking-group");
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

            return properties;

        }

    }

    final class PropriedadeProdutor implements PropriedadeMensageiro{

        private final String host;

        PropriedadeProdutor(final String host) {
            this.host = host;
        }

        @Override
        public Properties paraPropriedades() {

            final Properties properties = new Properties();

            properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
            properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

            return properties;

        }

    }

}
