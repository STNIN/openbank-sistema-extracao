FROM eclipse-temurin:17-alpine

RUN apk update

RUN apk upgrade

RUN adduser -D sistema-extracao

USER sistema-extracao

WORKDIR /home/sistema-extracao

COPY /target/*.jar sistema-extracao.jar

COPY run.sh run.sh

ENV WAIT_TIME 0

CMD ["sh", "run.sh"]