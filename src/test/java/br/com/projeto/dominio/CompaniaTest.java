package br.com.projeto.dominio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

class CompaniaTest {

    @Test
    @DisplayName("Compania possui produto com tipo emprestimo")
    void companiaPossuiProdutoTipoEmprestimo(){
        Assertions.assertTrue(
                new Compania("12345678901", List.of(new Produto(TipoProduto.EMPRESTIMO), new Produto(TipoProduto.IGNORACIA_SELETIVA) ) )
                        .companiaTemProduto(TipoProduto.EMPRESTIMO)
        );
    }

    @Test
    @DisplayName("Compania nao possui produto com tipo emprestimo")
    void companiaNaoPossuiProdutoTipoEmprestimo(){
        Assertions.assertFalse(
                new Compania("12345678901", List.of(new Produto(TipoProduto.IGNORACIA_SELETIVA) ) )
                        .companiaTemProduto(TipoProduto.EMPRESTIMO)
        );
    }

}