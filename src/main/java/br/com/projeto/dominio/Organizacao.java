package br.com.projeto.dominio;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Organizacao {

    public static Organizacao mock(final TipoProduto emprestimo){
        return new Organizacao(
                "nome",
                List.of(
                        new Compania(
                                "12345678912",
                                List.of(new Produto(emprestimo))
                        )
                )
        );
    }

    private final String name;

    private final List<Compania> companias;

    public Organizacao(final String name, final List<Compania> companias) {
        this.name = name;
        this.companias = companias;
    }

    public OrganizacaoRespostaDecorator filtrarCompaniasPorTipoProduto( final TipoProduto tipoProduto ){

        final List<CompaniaResposta> organizacao = companias.stream()
                .filter(compania -> compania.companiaTemProduto(tipoProduto))
                .map(Compania::companiaResposta)
                .toList();

        return new OrganizacaoRespostaDecorator() {
            @Override
            public void ifPresentOrElse(final Consumer<OrganizacaoResposta> encaminhar, final Consumer<String> encaminharErro) {
                if ( organizacao.isEmpty() ) encaminharErro.accept(nome());
                else encaminhar.accept(this);
            }

            @Override
            public String nome() {
                return name;
            }

            @Override
            public List<CompaniaResposta> companias() {
                return organizacao;
            }
        };
    }

    public interface OrganizacaoRespostaDecorator extends OrganizacaoResposta {

        void ifPresentOrElse(final Consumer<OrganizacaoResposta> encaminhar, final Consumer<String> encaminharErro );

    }

}
