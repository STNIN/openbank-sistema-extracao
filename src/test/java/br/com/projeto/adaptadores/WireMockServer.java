package br.com.projeto.adaptadores;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

final class WireMockServer {

    void stub( final String responseBody ){
        WireMock.stubFor(
                WireMock.get(WireMock.urlEqualTo( "/personal-loans/" ))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                        .withStatus(HttpStatus.OK.value())
                                        .withBody( responseBody )
                        )
        );
    }

}
