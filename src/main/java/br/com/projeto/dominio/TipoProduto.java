package br.com.projeto.dominio;

public enum TipoProduto {

    IGNORACIA_SELETIVA(""),
    EMPRESTIMO("EMPRESTIMO_CREDITO_PESSOAL");

    private final String tipo;

    TipoProduto(final String tipo){
        this.tipo = tipo;
    }

    public static TipoProduto de( final String tipo ){
        if( tipo != null && tipo.equals(EMPRESTIMO.tipo) ) return EMPRESTIMO;
        return IGNORACIA_SELETIVA;
    }

}
