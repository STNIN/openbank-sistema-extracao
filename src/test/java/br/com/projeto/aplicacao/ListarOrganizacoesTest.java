package br.com.projeto.aplicacao;

import br.com.projeto.dominio.Organizacao;
import br.com.projeto.dominio.OrganizacaoResposta;
import br.com.projeto.dominio.TipoProduto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

class ListarOrganizacoesTest {

    private static final String URL = "http://localhost:1234/personal-loans";

    @Test
    @DisplayName("Listar organizacoes com produtos do tipo emprestimo")
    void listarOrganizacoesComProdutosDoTipoEmprestimo(){

        AtomicInteger atomicInteger = new AtomicInteger();

        new ListarOrganizacoesAplicacao(
                ( url ) -> Optional.of( Organizacao.mock( TipoProduto.EMPRESTIMO ) ),
                new Evento() {
                    @Override
                    public void encaminharErro(final String nome) {
                        throw new IllegalCallerException();
                    }
                    @Override
                    public void encaminhar(final OrganizacaoResposta resposta) {
                        atomicInteger.getAndIncrement();
                    }
                }
        ).listarOrganizacoesComProdutoDoTipo( URL );

        Assertions.assertEquals(1, atomicInteger.get() );

    }

    @Test
    @DisplayName("Listar organizacoes com produtos de tipo diferente de emprestimo")
    void listarOrganizacoesComProdutosDoTipoDiferenteEmprestimo(){

        AtomicInteger atomicInteger = new AtomicInteger();

        new ListarOrganizacoesAplicacao(
                ( url ) -> Optional.of( Organizacao.mock( TipoProduto.IGNORACIA_SELETIVA ) ),
                new Evento() {
                    @Override
                    public void encaminharErro(final String nome) {
                        atomicInteger.getAndIncrement();
                    }
                    @Override
                    public void encaminhar(final OrganizacaoResposta resposta) {
                        throw new IllegalCallerException();
                    }
                }
        ).listarOrganizacoesComProdutoDoTipo( URL );

        Assertions.assertEquals(1, atomicInteger.get() );

    }

    @Test
    @DisplayName("Listar organizacoes sem organizacao recebida da api")
    void listarOrganizacoesSemOrganizacaoRecebidaDaApi(){

        AtomicInteger atomicInteger = new AtomicInteger();

        new ListarOrganizacoesAplicacao(
                ( url ) -> Optional.empty(),
                new Evento() {
                    @Override
                    public void encaminharErro(final String erro) {
                        atomicInteger.getAndIncrement();
                    }
                    @Override
                    public void encaminhar(final OrganizacaoResposta resposta) {
                        throw new IllegalCallerException();
                    }
                }
        ).listarOrganizacoesComProdutoDoTipo( URL );

        Assertions.assertEquals(1, atomicInteger.get() );

    }

}