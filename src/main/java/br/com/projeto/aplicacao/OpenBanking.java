package br.com.projeto.aplicacao;

import br.com.projeto.dominio.Organizacao;

import java.util.Optional;
import java.util.function.Consumer;

public interface OpenBanking {

    Optional<Organizacao> acessarApi( final String url );

}
