WAIT_TIME=$1
TOPIC_OPENBANKING=$2

sleep "${WAIT_TIME}"

create_topic_open_banking(){
  docker-compose -f ../../docker-compose.yml \
  exec kafka kafka-topics --create --topic "$TOPIC_OPENBANKING" --partitions 1 --replication-factor 1 --bootstrap-server kafka:8082
}

populate_kafka_open_banking_urls(){
  declare -a loans=(
    "localhost:15000/loans-service/product-type/emprestimo-credito-pessoal"
    "localhost:15000/loans-service/product-type/emprestimo-credito-juridico"
  )
  for url in "${loans[@]}"
  do
    docker-compose -f ../../docker-compose.yml \
    exec kafka bash -c "echo '${url}' | kafka-console-producer --request-required-acks 1 --broker-list kafka:28082 --topic $TOPIC_OPENBANKING"
    echo "Producing : ${url}"
  done
}

create_topic_open_banking
populate_kafka_open_banking_urls