# OpenBank Sistema Extração

<img alt="extract" src="extract.png" width="100">

---

## Objetivo

* Listar compania de instituições financeiras cujo tipo de produto é EMPRESTIMO PESSOAL.

## Regras

1. type deve ser EMPRESTIMO_CREDITO_PESSOAL
2. deve paginar e trazer todas as companias

## Entrada

* Consumindo topico de um mensageiro

~~~json
{
  "end-point" : String
}
~~~

### Saida

* Encaminhar para um topico de mensageiro

~~~json
{
  "brand" : String,
  "companies" : [
    "cnpj" : String
  ]
}
~~~

## Saida Erro

* Encaminhar para um topico de mensageiro

~~~json
{
  "brand" : String,
  "mensagem" : String
}
~~~

---

## Modelagem UML Projeto

* Diagrama de Componente

  ![Component](ComponentDiagram.png)

* Diagrama de Atividade

  ![Activity](Activity.png)

* Diagrama de Classes

  ![Class](Class.png)

* Diagrama de Classes Adaptadores

  ![Class](Adaptadores.png)
