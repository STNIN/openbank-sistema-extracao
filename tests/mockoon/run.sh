PORT=$1
MOCKOONFILE=$2
docker run -d --mount type=bind,source=$(pwd)/"$MOCKOONFILE",target=/data,readonly -p "$PORT":"$PORT" mockoon/cli:latest -d data -i 0 -p "$PORT"