package br.com.projeto.adaptadores;

import br.com.projeto.adaptadores.mensageiro.consumidor.MensageiroConsumidorAdaptador;
import br.com.projeto.adaptadores.mensageiro.produtor.MensageiroProdutorAdaptador;
import br.com.projeto.adaptadores.open.banking.web.OpenBankingAdaptador;
import br.com.projeto.aplicacao.ListarOrganizacoesAplicacao;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.*;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Collections;
import java.util.Map;

@DirtiesContext
@ActiveProfiles( value = "test" )
@AutoConfigureWireMock(port = 0)
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })
final class FalhaDeserializarRequisicaoIntegrationTest {

    private static final String EMBEDDED_KAFKA_HOST = "localhost:9092";

    private static final String OPEN_BANKING_URL_TOPIC = "open-banking-urls-test";
    private static final String BRAND_TOPIC = "brands-test";
    private static final String ERROR_TOPIC = "error-test";

    private static final String WIRE_MOCK_HOST = "localhost";
    private static final Integer WIRE_MOCK_PORT = 8089;

    private final KafkaConsumer<String, String> consumer = new KafkaConsumer<>(
            new PropriedadeMensageiro.PropriedadeConsumidor( EMBEDDED_KAFKA_HOST ).paraPropriedades()
    );

    private final KafkaProducer<String, String> producer = new KafkaProducer<>(
            new PropriedadeMensageiro.PropriedadeProdutor( EMBEDDED_KAFKA_HOST ).paraPropriedades()
    );

    private static WireMockServer wireMockServer;

    @BeforeEach
    void produzirUrlEmprestimo(){

        final ProducerRecord<String, String> record = new ProducerRecord<>(
                OPEN_BANKING_URL_TOPIC,
                "http://" + WIRE_MOCK_HOST + ":" + WIRE_MOCK_PORT + "/personal-loans/"
        );

        producer.send(record, (success, failure) -> {
            if(failure != null )
                failure.printStackTrace();
        });

        wireMockServer = new WireMockServer( new WireMockConfiguration().port(WIRE_MOCK_PORT));

        wireMockServer.start();

        WireMock.configureFor(WIRE_MOCK_HOST, WIRE_MOCK_PORT);

    }

    @Test
    @DisplayName("Falha ao deserializar a requisicao")
    public void falhaDeserializacao() throws IOException {

        new br.com.projeto.adaptadores.WireMockServer().stub(
                Files.readString(
                        ResourceUtils
                                .getFile("src/test/resources/br/com/projeto/adaptadores/response-open-banking-api-error-fields.json")
                                .toPath()
                )
        );

        consumer.subscribe(Collections.singletonList(OPEN_BANKING_URL_TOPIC));

        new MensageiroConsumidorAdaptador( consumer,
                new ListarOrganizacoesAplicacao(
                        new OpenBankingAdaptador( new RestTemplate() ),
                        new MensageiroProdutorAdaptador(producer, Map.of(
                                "topic-response-success", BRAND_TOPIC,
                                "topic-response-error", ERROR_TOPIC
                        ) )
                )
        ).consumir();

        consumer.subscribe(Collections.singletonList(ERROR_TOPIC));

        final ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));

        Assertions.assertFalse(records.isEmpty());

        Assertions.assertEquals("Open Banking Api não proveu uma organização.", records.iterator().next().value() );

    }

    @AfterEach
    void stopWireMock(){
        wireMockServer.stop();
    }

}
