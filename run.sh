#!/bin/bash

sleep ${WAIT_TIME}

java -jar sistema-extracao.jar ${KAFKA_HOST} ${OPEN_BANKING_TOPIC} ${SUCCESS_MESSAGE_TOPIC} ${ERROR_TOPIC}