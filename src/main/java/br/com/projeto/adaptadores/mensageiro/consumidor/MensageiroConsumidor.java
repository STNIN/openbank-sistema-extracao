package br.com.projeto.adaptadores.mensageiro.consumidor;

public interface MensageiroConsumidor {

    void consumir();

}
