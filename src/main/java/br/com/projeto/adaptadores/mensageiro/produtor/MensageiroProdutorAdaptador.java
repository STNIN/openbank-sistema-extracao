package br.com.projeto.adaptadores.mensageiro.produtor;

import br.com.projeto.aplicacao.Evento;
import br.com.projeto.dominio.OrganizacaoResposta;
import com.google.gson.Gson;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public final class MensageiroProdutorAdaptador implements Evento {

    private final KafkaProducer<String, String> kafkaProducer;

    private final Map<String, String> topics;

    public MensageiroProdutorAdaptador(final KafkaProducer<String, String> kafkaProducer, final Map<String, String> topics) {
        this.kafkaProducer = kafkaProducer;
        this.topics = topics;
    }

    @Override
    public void encaminharErro(final String erro) {
        produzir( topics.get("topic-response-error"), erro );
    }

    @Override
    public void encaminhar(final OrganizacaoResposta resposta) {
        produzir(
                topics.get("topic-response-success"),
                new Gson()
                        .toJson( new Brand(resposta) )
        );
    }

    private void produzir(final String topico, final String mensagem) {

        final ProducerRecord<String, String> record = new ProducerRecord<>(topico, mensagem);

        try {

            kafkaProducer.send(record, (success, failure) -> {
                if (failure != null) failure.printStackTrace();
            }).get();

        } catch (InterruptedException | ExecutionException | IllegalStateException e) {

            e.printStackTrace();

        }

    }

    private static final class Brand {

        private final String name;

        private final List<Company> companies;

        private Brand(final OrganizacaoResposta organizacaoResposta) {

            this.name = organizacaoResposta.nome();

            this.companies = organizacaoResposta
                    .companias()
                    .stream()
                    .map(companiaResposta -> new Company(companiaResposta.cnpj()))
                    .toList();

        }

        private static final class Company {

            private final String cnpj;

            private Company(final String cnpj) {
                this.cnpj = cnpj;
            }

        }

    }


}
