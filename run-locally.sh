#!/bin/bash
command=$1
quantityMessages=$2

export JAVA_HOME=~/.jdks/openjdk-17/

build_project(){
  ./mvnw clean package
}

up_docker_compose(){
  docker-compose -f ./docker/docker-compose.yml up -d
}

down_docker_compose(){
  docker-compose -f ./docker/docker-compose.yml down
}

clean_docker(){
  docker rm $(docker ps -a -q)
  docker rmi $(docker images -q)
  docker volume prune
  docker system prune --all --force --volumes
}

create_topic_open_banking(){
  docker-compose -f ./docker/docker-compose.yml \
  exec kafka kafka-topics --create --topic open-banking-urls --partitions 1 --replication-factor 1 --bootstrap-server kafka:8082
}

populate_kafka_open_banking_urls(){
  data=$(curl https://data.directory.openbankingbrasil.org.br/participants)
  personalLoans=$(grep -oP -- '[a-zA-Z0-9\/\-.:_]*personal\-loans[a-zA-Z0-9\/\-.:_]*' <<< $data)
  loans=($(echo $personalLoans | tr " " "\n" | sort -u))
  for url in "${loans[@]}"
  do
    docker-compose -f ./docker/docker-compose.yml \
    exec kafka bash -c "echo '${url}' | kafka-console-producer --request-required-acks 1 --broker-list kafka:28082 --topic open-banking-urls"
    echo "Producing : ${url}"
  done
}

consume_kafka_topic(){
  docker-compose -f ./docker/docker-compose.yml \
  exec kafka kafka-console-consumer --bootstrap-server kafka:28082 --topic open-banking-urls --from-beginning --max-messages $quantityMessages
}

read_extracao_erro(){
  docker-compose -f ./docker/docker-compose.yml \
  exec kafka kafka-console-consumer --bootstrap-server kafka:28082 --topic extracao-erro --from-beginning --max-messages $quantityMessages
}

read_extracao_sucesso(){
  docker-compose -f ./docker/docker-compose.yml \
  exec kafka kafka-console-consumer --bootstrap-server kafka:28082 --topic extracao-sucesso --from-beginning --max-messages $quantityMessages
}

case $command in
  "start")
    echo "Starting ..."
    build_project
    up_docker_compose
  ;;
  "end")
    down_docker_compose
    clean_docker
  ;;
  "create-topic")
    create_topic_open_banking
  ;;
  "populate-kafka")
    sleep 5
    populate_kafka_open_banking_urls
    echo "Kakfa populated"
  ;;
  "read-kafka")
    consume_kafka_topic
  ;;
  "read-extracao-error")
    read_extracao_erro
  ;;
  "read-extracao-sucesso")
    read_extracao_sucesso
  ;;
  *)
    echo "Command not exists!!"
  ;;
esac