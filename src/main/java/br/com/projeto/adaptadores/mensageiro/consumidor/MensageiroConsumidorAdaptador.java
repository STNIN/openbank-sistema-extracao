package br.com.projeto.adaptadores.mensageiro.consumidor;

import br.com.projeto.aplicacao.ListarOrganizacoes;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.stream.StreamSupport;

public final class MensageiroConsumidorAdaptador implements MensageiroConsumidor {

    public MensageiroConsumidorAdaptador(final KafkaConsumer<String, String> kafkaConsumer, final ListarOrganizacoes listarOrganizacoes) {
        this.kafkaConsumer = kafkaConsumer;
        this.listarOrganizacoes = listarOrganizacoes;
    }

    private final KafkaConsumer<String, String> kafkaConsumer;

    private final ListarOrganizacoes listarOrganizacoes;

    @Override
    public void consumir() {

        StreamSupport
                .stream(kafkaConsumer.poll(Duration.ofMillis(1000)).spliterator(), false)
                .findFirst()
                .ifPresent( mensagem ->
                        listarOrganizacoes
                                .listarOrganizacoesComProdutoDoTipo( mensagem.value() )
                );

    }

}
