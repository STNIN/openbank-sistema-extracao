package br.com.projeto.dominio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

class OrganizacaoTest {

    @Test
    @DisplayName("Organizacao com compania que possui produtos do tipo emprestimo")
    void organizacaoComCompaniaQuePossuiProdutosTipoEmprestimo(){

        final Organizacao.OrganizacaoRespostaDecorator organizacaoRespostaDecorator = Organizacao
                .mock(TipoProduto.EMPRESTIMO)
                .filtrarCompaniasPorTipoProduto(TipoProduto.EMPRESTIMO);

        final AtomicInteger atomicInteger = new AtomicInteger();

        organizacaoRespostaDecorator.ifPresentOrElse(
                organizacaoResposta -> atomicInteger.getAndIncrement(),
                error -> { throw new IllegalCallerException(); }
        );

        Assertions.assertEquals( 1, atomicInteger.get() );

        Assertions.assertEquals("nome", organizacaoRespostaDecorator.nome() );

        final List<CompaniaResposta> companias = organizacaoRespostaDecorator.companias();
        Assertions.assertEquals(1, companias.size() );
        Assertions.assertEquals(
                List.of( "12345678912" ),
                companias
                        .stream()
                        .map(CompaniaResposta::cnpj)
                        .toList()
        );
    }

    @Test
    @DisplayName("Organizacao com compania que nao possui produtos do tipo emprestimo")
    void organizacaoComCompaniaQueNaoPossuiProdutosTipoEmprestimo(){

        final Organizacao.OrganizacaoRespostaDecorator organizacaoRespostaDecorator = Organizacao
                .mock(TipoProduto.EMPRESTIMO)
                .filtrarCompaniasPorTipoProduto(TipoProduto.IGNORACIA_SELETIVA);

        final AtomicInteger atomicInteger = new AtomicInteger();

        organizacaoRespostaDecorator.ifPresentOrElse(
                organizacaoResposta -> { throw new IllegalCallerException(); },
                error -> atomicInteger.getAndIncrement()
        );

        Assertions.assertEquals( 1, atomicInteger.get() );

        Assertions.assertEquals("nome", organizacaoRespostaDecorator.nome() );

        final List<CompaniaResposta> companias = organizacaoRespostaDecorator.companias();
        Assertions.assertEquals(0, companias.size() );
    }

}