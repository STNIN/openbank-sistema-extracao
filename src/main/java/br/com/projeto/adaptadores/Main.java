package br.com.projeto.adaptadores;

import br.com.projeto.adaptadores.mensageiro.consumidor.MensageiroConsumidor;
import br.com.projeto.adaptadores.mensageiro.consumidor.MensageiroConsumidorAdaptador;
import br.com.projeto.adaptadores.mensageiro.produtor.MensageiroProdutorAdaptador;
import br.com.projeto.adaptadores.open.banking.web.OpenBankingAdaptador;
import br.com.projeto.aplicacao.ListarOrganizacoesAplicacao;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Map;

@SpringBootApplication
public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        SpringApplication.run(Main.class, args);

        final String host = args[0];

        final String topicOpenBankingApis = args[1];
        final String topicResponseSuccess = args[2];
        final String topicResponseError = args[3];

        final KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(
                new PropriedadeMensageiro.PropriedadeConsumidor( host ).paraPropriedades()
        );
        kafkaConsumer.subscribe(Collections.singletonList(topicOpenBankingApis));

        final MensageiroConsumidor mensageiroConsumidor = new MensageiroConsumidorAdaptador(
                kafkaConsumer,
                new ListarOrganizacoesAplicacao(
                        new OpenBankingAdaptador( new RestTemplateWrapper() ),
                        new MensageiroProdutorAdaptador(
                                new KafkaProducer<>(
                                        new PropriedadeMensageiro.PropriedadeProdutor( host ). paraPropriedades()
                                ),
                                Map.of(
                                        "topic-response-success", topicResponseSuccess,
                                        "topic-response-error", topicResponseError
                                )
                        )
                )
        );

        while( true ) { mensageiroConsumidor.consumir(); }

    }

    private static final class RestTemplateWrapper extends RestTemplate {

        RestTemplateWrapper() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

            super(
                    new HttpComponentsClientHttpRequestFactory(
                            HttpClients
                                    .custom()
                                    .setSSLSocketFactory(
                                            new SSLConnectionSocketFactory(
                                                    SSLContexts
                                                            .custom()
                                                            .loadTrustMaterial((x509Certificates, s) -> true)
                                                            .build(),
                                                    new NoopHostnameVerifier()
                                            )
                                    ).build()
                    )
            );

        }

    }

}
