package br.com.projeto.dominio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ProdutoTest {

    @Test
    @DisplayName("Tipo do produto igual emprestimo")
    void tipoProdutoIgualEmprestimo(){
        final TipoProduto emprestimo = TipoProduto.EMPRESTIMO;
        Assertions.assertTrue(
                new Produto(emprestimo).produtoEDoTipo(emprestimo)
        );
    }

    @Test
    @DisplayName("Tipo do produto diferente emprestimo")
    void tipoProdutoDiferenteEmprestimo(){
        Assertions.assertFalse(
                new Produto(TipoProduto.EMPRESTIMO).produtoEDoTipo(TipoProduto.IGNORACIA_SELETIVA)
        );
    }

}