package br.com.projeto.dominio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TipoProdutoTest {

    @Test
    @DisplayName("Tipo nulo")
    void tipoNulo(){
        Assertions.assertEquals(TipoProduto.IGNORACIA_SELETIVA, TipoProduto.de(null));
    }

    @Test
    @DisplayName("Tipo vazio")
    void tipoVazio(){
        Assertions.assertEquals(TipoProduto.IGNORACIA_SELETIVA, TipoProduto.de(""));
    }

    @Test
    @DisplayName("Tipo emprestimo")
    void tipoEmprestimo(){
        Assertions.assertEquals(TipoProduto.EMPRESTIMO, TipoProduto.de("EMPRESTIMO_CREDITO_PESSOAL"));
    }

}
