package br.com.projeto.aplicacao;

import br.com.projeto.dominio.TipoProduto;

public class ListarOrganizacoesAplicacao implements ListarOrganizacoes{

    private final OpenBanking openBanking;

    private final Evento evento;

    public ListarOrganizacoesAplicacao(final OpenBanking openBanking, final Evento evento){
        this.openBanking = openBanking;
        this.evento = evento;
    }

    @Override
    public void listarOrganizacoesComProdutoDoTipo( final String url ) {

        openBanking
                .acessarApi( url )
                .ifPresentOrElse(
                        organizacao -> organizacao
                                .filtrarCompaniasPorTipoProduto(TipoProduto.EMPRESTIMO)
                                .ifPresentOrElse( evento::encaminhar, evento::encaminharErro ),
                        () -> evento.encaminharErro("Open Banking Api não proveu uma organização.")
                );

    }

}