package br.com.projeto.adaptadores.open.banking.web;

import br.com.projeto.aplicacao.OpenBanking;
import br.com.projeto.dominio.Compania;
import br.com.projeto.dominio.Organizacao;
import br.com.projeto.dominio.Produto;
import br.com.projeto.dominio.TipoProduto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Optional;

public final class OpenBankingAdaptador implements OpenBanking {

    public OpenBankingAdaptador(final RestTemplate restTemplate) {

        this.restTemplate = restTemplate;

        this.objectMapper = new ObjectMapper();

        objectMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );

    }

    private final ObjectMapper objectMapper;

    private final RestTemplate restTemplate;

    @Override
    public Optional<Organizacao> acessarApi(final String url) {

        try{

            return Optional.of(
                    objectMapper.readValue(
                            restTemplate.getForEntity( URI.create( url ), String.class )
                            .getBody(),
                            Response.class
                    ).paraOrganizacao()
            );

        }catch (final ResourceAccessException | HttpClientErrorException | JsonProcessingException exception ){

            return Optional.empty();

        }

    }

    private static final class Response {

        private Data data;

        public void setData(final Data data) {
            this.data = data;
        }

        Organizacao paraOrganizacao() {
            return data.getBrand();
        }

        private static final class Data {

            private Brand brand;

            private void setBrand(final Brand brand) {
                this.brand = brand;
            }

            private Organizacao getBrand() {
                return brand.paraOrganizacao();
            }

            private static final class Brand {

                public void setName(final String name) {
                    this.name = name;
                }

                private String name;

                public void setCompanies(final List<Company> companies) {
                    this.companies = companies;
                }

                private List<Company> companies;

                Organizacao paraOrganizacao() {
                    return new Organizacao(
                            name,
                            companies
                                    .stream()
                                    .map(Company::paraCompania)
                                    .toList()
                    );
                }

                private static final class Company {

                    public void setCnpjNumber(final String cnpjNumber) {
                        this.cnpjNumber = cnpjNumber;
                    }

                    private String cnpjNumber;

                    public void setPersonalLoans(final List<Product> personalLoans) {
                        this.personalLoans = personalLoans;
                    }

                    private List<Product> personalLoans;

                    Compania paraCompania() {
                        return new Compania(
                                cnpjNumber,
                                personalLoans
                                        .stream()
                                        .map(Product::paraProduto)
                                        .toList()
                        );
                    }

                    private static final class Product {

                        public void setType(final String type) {
                            this.type = type;
                        }

                        private String type;

                        Produto paraProduto() {
                            return new Produto(TipoProduto.de(type));
                        }

                    }

                }

            }

        }

    }

}
