package br.com.projeto.dominio;

import java.util.List;

public class Compania {

    private final String cnpj;

    private final List<Produto> produtos;

    public Compania(final String cnpj, final List<Produto> produtos) {
        this.cnpj = cnpj;
        this.produtos = produtos;
    }

    boolean companiaTemProduto( final TipoProduto tipoProduto ){
        return produtos
                .stream()
                .anyMatch( produto -> produto.produtoEDoTipo( tipoProduto ) );
    }

    CompaniaResposta companiaResposta(){
        return () -> cnpj;
    }

}
