package br.com.projeto.aplicacao;

import br.com.projeto.dominio.OrganizacaoResposta;

public interface Evento {

    void encaminharErro(final String nome);

    void encaminhar(final OrganizacaoResposta resposta);

}
