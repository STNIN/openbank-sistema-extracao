package br.com.projeto.dominio;

public class Produto {

    public Produto(final TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    private final TipoProduto tipoProduto;

    boolean produtoEDoTipo( final TipoProduto tipoProduto ){
        return this.tipoProduto.equals( tipoProduto );
    }

}
