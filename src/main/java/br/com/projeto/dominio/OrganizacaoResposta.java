package br.com.projeto.dominio;

import java.util.List;

public interface OrganizacaoResposta {

    String nome();

    List<CompaniaResposta> companias();

}
